from dataclasses import dataclass, field
import functools
from typing import Any, Tuple
import json
import hashlib
import random
from datetime import datetime
import logging
import io
import argparse
import requests
from bs4 import BeautifulSoup
from dacite.core import from_dict


ACCSO_MENSCHEN_URL = r'https://accso.de/menschen'

HISTORY_URL = r'https://jlorek.gitlab.io/motd/history.json'
HISTORY_FILE = r'public/history.json'

MOTD_URL = r'https://jlorek.gitlab.io/motd/motd.json'
MOTD_FILE = r'public/motd.json'

LOG_FILE = r'public/log.txt'


class StafferKey(str):
    """Strongly typed key for Staffer"""


@dataclass
class Staffer:
    """A singular staff member"""
    name: str
    position: str
    image: str
    motd_dates: list[str] = field(default_factory=list)


class Staff(dict[StafferKey, Staffer]):
    """The staff in its entirety"""


class History(dict[StafferKey, Staffer]):
    """"All staffers that have been motd at least once"""


def save_json(filename: str, data: Any) -> None:
    with open(filename, 'w', encoding='utf_8') as file:
        json.dump(data, file, indent=2)


def save_history(history: History):
    raw = {k: v.__dict__ for k, v in history.items()}
    save_json(HISTORY_FILE, raw)
    LOGGER.debug("Wrote history file")


def save_motd(modt: Staffer):
    raw = modt.__dict__
    save_json(MOTD_FILE, raw)
    LOGGER.debug("Wrote modt file")


def load_history() -> History:
    response = requests.get(HISTORY_URL)
    raw_history = json.loads(response.text) if response.ok else {}
    history = History(((k, from_dict(Staffer, v))
                      for k, v in raw_history.items()))
    LOGGER.debug("Loaded %d historical motd across %d staff members from history file",
                 sum((len(item.motd_dates) for item in history.values())), len(history))
    return history


def update_history(history: History, modt: Staffer) -> None:
    key = get_key(modt)
    if key not in history:
        history[key] = modt
    modt.motd_dates.append(datetime.today().strftime('%Y-%m-%d'))


def get_key(staffer: Staffer):
    return StafferKey(hashlib.md5(staffer.name.encode('utf-8')).hexdigest())


def load_staff() -> Staff:
    page = requests.get(ACCSO_MENSCHEN_URL)
    soup = BeautifulSoup(page.content, "html.parser")
    staff_nodes = soup.find_all("div", class_="staff")

    result = {}
    for staff_node in staff_nodes:
        info_node = staff_node.find("div", class_="info")
        name = info_node.find("div", class_="name").text
        position = info_node.find("div", class_="position").text
        image = staff_node.find("img").attrs['data-lazy-src']

        staffer = Staffer(name, position, image)
        key = get_key(staffer)
        result[key] = staffer

    staff = Staff(result)
    LOGGER.debug("Found %d staff members", len(staff))
    return staff


def update_staff_from_history(staff: Staff, history: History) -> None:
    for staffer_key, staffer in staff.items():
        if staffer_key in history:
            staffer.motd_dates = history[staffer_key].motd_dates


def load_motd() -> Staffer | None:
    response = requests.get(MOTD_URL)
    if not response.ok:
        return None
    try:
        raw_motd = json.loads(response.text)
        motd = from_dict(Staffer, raw_motd)
        return motd
    except:
        return None


def pick_motd(staff: Staff) -> Staffer:
    min_motd_count = min((len(p.motd_dates) for p in staff.values()))
    choices = sorted((k for k, v in staff.items()
                      if len(v.motd_dates) == min_motd_count))
    choice = random.choice(choices)
    motd = staff[choice]
    LOGGER.debug("Picked the new motd '%s' out of %d eligible staff members",
                 motd.name, len(choices))
    return motd


def is_todays_motd(last_motd: Staffer):
    return last_motd and last_motd.motd_dates and sorted(last_motd.motd_dates)[-1] == datetime.today().strftime('%Y-%m-%d')


def with_logging(func):
    """Decorator that provides the logging infrastructure"""
    @functools.wraps(func)
    def logging_wrapper(*args, **kwargs):
        global LOGGER
        stream = io.StringIO()
        LOGGER = logging.getLogger()
        LOGGER.setLevel('DEBUG')
        handler = logging.StreamHandler(stream)
        formatter = logging.Formatter(
            "%(asctime)s - %(name)s - %(levelname)s - %(message)s")
        handler.setFormatter(formatter)
        LOGGER.addHandler(handler)

        try:
            LOGGER.info("Executing '%s' ...", func.__name__)
            func(*args, **kwargs)
            LOGGER.info("Execution of '%s' completed", func.__name__)
        finally:
            handler.flush()
            stream.flush()
            with open(LOG_FILE, 'w', encoding='utf_8') as file:
                file.write(stream.getvalue())
    return logging_wrapper


@with_logging
def command__pick_motd(args):
    last_motd = load_motd()
    staff = load_staff()
    history = load_history()
    update_staff_from_history(staff, history)

    if last_motd and is_todays_motd(last_motd) and not args.force:
        LOGGER.debug(
            "Today is still %s and %s is still motd. Not picking a new motd until tomorrow.", last_motd.motd_dates[-1], last_motd.name)
        motd = last_motd
    else:
        motd = pick_motd(staff)
        update_history(history, motd)

    save_history(history)
    save_motd(motd)


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument("--force", action='store_true')
    parsed = parser.parse_args()
    command__pick_motd(parsed)
