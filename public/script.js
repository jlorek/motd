var open = false;
var closeTimeout = null;
const audioUrl = "fanfare.m4a";
const audio = new Audio(audioUrl);

window.onload = async function () {
  const motdUrl = "https://jlorek.gitlab.io/motd/motd.json";
  var motdJson = await fetch(motdUrl);
  const motd = await motdJson.json();

  document.getElementById("motd_name").innerText = motd.name;
  document.getElementById("motd_image").setAttribute("src", motd.image);

  document.onclick = function () {
    if (open) {
      close_curtain();
    } else {
      open_curtain();
    }
  };
};

function open_curtain() {
  audio.currentTime = 0;
  audio.play();
  $("#curtain1").animate({ width: 20 }, 1000);
  $("#curtain2").animate({ width: 20 }, 1000);
  open = true;
  closeTimeout = window.setTimeout(function () {
    close_curtain();
  }, 1000 * 6.5);
}

function close_curtain() {
  clearTimeout(closeTimeout);
  audio.currentTime = 0;
  audio.pause();
  $("#curtain1").animate({ width: "50%" }, 1000);
  $("#curtain2").animate({ width: "50%" }, 1000);
  open = false;
}
